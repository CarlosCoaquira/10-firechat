import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Mensaje } from '../interfaces/mensaje.interface';

import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';

// export interface Item { name: string; }

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  // items: Observable<Item[]>;

  public chats: Mensaje[] = [];
  public usuario: any = { };

  constructor(private afs: AngularFirestore,
              public auth: AngularFireAuth) {

                this.auth.authState.subscribe( user => {
                  console.log('Estado del usuario: ', user);

                  if (!user) {
                    return;
                  }

                  this.usuario.nombre = user.displayName;
                  this.usuario.uid = user.uid;


                });

  }

  login(proveedor: string) {
    if (proveedor==='google') {
      this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }

    if (proveedor==='twiter') {
      this.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider())
      .then( resp => {
        console.log(resp);
      } ).catch( err => {
        console.log(err);
      });
    }


  }

  logout() {
    this.auth.signOut();
    this.usuario = {};
  }

  // addItem(item: Item) {
  //   this.itemsCollection.add(item);
  // }

  cargarMensajes() {
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha','desc')
                                                                            .limit(5) );
    // this.chats = this.itemsCollection.valueChanges();

    return this.itemsCollection.valueChanges().pipe(
      map( (mensajes: Mensaje[]) => {
        console.log(mensajes);
        // this.chats = mensajes;
        this.chats = [];

        for (const mensaje of mensajes) {
          this.chats.unshift(mensaje); // inserta al principio del array
        }

        return this.chats;
      } )
    );
  }

  agregarMensaje( texto: string) {

    // TODO falta el uid del usuario
    const mensaje: Mensaje = {
      nombre: this.usuario.nombre,
      mensaje: texto,
      fecha: new Date().getTime(),
      uid: this.usuario.uid
    };

    return this.itemsCollection.add(mensaje);

  }

}

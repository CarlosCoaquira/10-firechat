// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBNZV4_ZCXeXkZKmoZF-SgdOdnNrdh50jU',
    authDomain: 'firechat-12f87.firebaseapp.com',
    databaseURL: 'https://firechat-12f87.firebaseio.com',
    projectId: 'firechat-12f87',
    storageBucket: 'firechat-12f87.appspot.com',
    messagingSenderId: '54006619331',
    appId: '1:54006619331:web:7c7c23d36473fced341676'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
